import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);

    app.enableCors({
      origin:"http://localhost:4200",
      credentials:true
    })

    const config = new DocumentBuilder()
      .setTitle('Tpro Backend')
      .setDescription('Backen Api for Tpro Test')
      .setVersion('1.0')
      .addTag('tpro')
      .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    app.use(cookieParser());

    await app.listen(3000);  
}
bootstrap();
