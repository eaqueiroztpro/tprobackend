import { Module } from '@nestjs/common';
import { AuthModule } from './modules/auth/auth.module';
import { TodosModule } from './modules/todos/todos.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [
              TodosModule,
              UsersModule,
              AuthModule
           ],
  controllers: [],
  providers: [],
})
export class AppModule {}
