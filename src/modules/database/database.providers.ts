import { Sequelize } from 'sequelize-typescript';
import { TodoItem } from '../todos/todos.entity';
import { User } from '../users/users.entity';


export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'postgres',
        host: 'db',
        port: 5432,
        username: 'postgres',
        password: 'postgres',
        database: 'tpro',
      });
      sequelize.addModels([TodoItem, User]);
      await sequelize.sync();
      return sequelize;
    },
  },
];