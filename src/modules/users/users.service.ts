import { Injectable, Inject } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { User } from './users.entity';

@Injectable()
export class UsersService {
  myUsers:Array<any>=[];

  constructor(
  @Inject('USERS_REPOSITORY')
  private userRepository: typeof User
  ){

  }

  registerUser(user:User) {
    return this.userRepository.create({
      name: user.name,
      email:user.email,
      password: user.password
    });    
  }

  getUser(userEmail:string) {    
    return this.userRepository.findOne({where:{email: userEmail}});
  }
}


export class Login{    
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;
}

