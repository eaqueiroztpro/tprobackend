import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { DatabaseModule } from '../database/database.module';
import { UsersController } from './users.controller';
import { userProviders } from './users.providers';
import { UsersService } from './users.service';

@Module({
  imports: [
    JwtModule.register({
                  secret: 'secretKey',
                  signOptions: { expiresIn: '360s' },
                }),
    DatabaseModule
  ],
  controllers: [UsersController],
  providers: [
    UsersService,
    ...userProviders,
  ],
})
export class UsersModule {}