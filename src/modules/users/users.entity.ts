import { ApiProperty } from "@nestjs/swagger";
import { Column, Table, Model } from "sequelize-typescript";

@Table
export class User extends Model{    
  @ApiProperty()
  @Column
  name: string;

  @ApiProperty()
  @Column
  email: string;

  @ApiProperty()
  @Column
  password: string;
}
