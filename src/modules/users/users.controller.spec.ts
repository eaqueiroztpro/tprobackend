import { createMocks, Mocks } from 'node-mocks-http';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { Login , UsersService} from './users.service';


describe('UsersController', () => {
  let usersController: UsersController;
  
  const fakeJwtService = {
    provide: JwtService,
    useFactory: ()=>({
      verifyAsync: jest.fn(()=>{} )
    })    
  }
  
  const fakeUsersService = {      
      provide: UsersService,
      useFactory: ()=>({
        getUser: jest.fn(()=>'user successfully authenticated!' )
      })    
  }

  const fakeRepo = {
    provide: 'USERS_REPOSITORY',
    useValue: {
      emit: jest.fn(),
    },
  } 
  
  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({      
      controllers: [UsersController],
      providers: [ fakeUsersService,
                  fakeJwtService,
                  fakeRepo                    
                 ], 
    }).compile();

    usersController = app.get<UsersController>(UsersController);
  });

  describe('root', () => {
    //const req = mocks.createRequest()
    //req.res = mocks.createResponse()    
    it('should be defined', () => {
      expect(usersController).toBeDefined();
    });

  });
});
