import { BadRequestException, Body, Controller, Get, Post, Res, Req, UnauthorizedException } from '@nestjs/common';
import { Login, UsersService } from './users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { Response,Request} from 'express';
import { User } from './users.entity';
import { addAssociation } from 'sequelize-typescript';

@ApiTags('user')
@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService,
             private jwtService: JwtService
             ) {    
  }

  @Post('register')
  async register( @Body() user:User){
      if(!user || !user.email || !user.password || !user.name)
        throw new BadRequestException("email, name and password are required")

        user.password = await bcrypt.hash(user.password, 12);
        return this.usersService.registerUser(user);        
    }

  @Post('login')
  async login( 
    @Body() login:Login,
    @Res({passthrough:true}) reply: Response    
    ){
      console.log("it got inside");

      if(!login || !login.email || !login.password)
        throw new BadRequestException("email and password are required")

      var user = await this.usersService.getUser(login.email);      
      

      if(!user)
        throw new BadRequestException("invalid Credentials")

      if(!await bcrypt.compare(login.password, user.password))
        throw new UnauthorizedException("invalid Credentials");

      const jwt = await this.jwtService.signAsync({name: user.name});

      reply.clearCookie('jwt');
      reply.cookie('jwt',jwt,{httpOnly:true});
      return {
        jwt:jwt
      };
    }

  @Get('getLoggedUser')
  async getLoggedUser(@Req() request: Request){
    try {
      console.log("inside getLoggedUser");
      const cookie = request.cookies['jwt'];

      console.log(cookie);
      const data = await this.jwtService.verifyAsync(cookie);      

      if(!data){
        throw new UnauthorizedException();
      }
      let d = data as User;
      d.password='###';

      return data;  
    } 
    catch (error) {
      throw new UnauthorizedException();
    } 
  }

  @Post('logout')
  async logout(
    @Res({passthrough:true}) reply: Response){
      reply.clearCookie('jwt');
      return {
        message:"user successfully logged out!"
      };
  }  
}
