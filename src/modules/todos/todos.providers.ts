import { TodoItem } from './todos.entity';

export const todoProviders = [
  {
    provide: 'TODOS_REPOSITORY',
    useValue: TodoItem,
  },
];