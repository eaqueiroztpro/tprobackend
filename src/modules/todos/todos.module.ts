import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { TodosController } from './todos.controller';
import { todoProviders } from './todos.providers';
import { TodosService } from './todos.service';

@Module({
  imports: [DatabaseModule],
  controllers: [TodosController],
  providers: [
    TodosService,
    ...todoProviders,
  ],
})
export class TodosModule {}