import { Body, Controller, Get, Delete, Put, Post, Res, Req, UnauthorizedException, Param, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { JwtGuard } from '../auth/jwt.guard';
import { TodoItem } from './todos.entity';
import { TodosService } from './todos.service';

@ApiTags('todos')
@Controller('todos')
export class TodosController {
    constructor(private readonly todoService: TodosService){

    }   
    
    @Get('getAll')
    async getAll(){
        return this.todoService.getAllTodos();          
       }       

    @UseGuards(JwtGuard)
    @Post('create')
    async create(
      @Body() todo: TodoItem
      ){
          this.todoService.createTodo(todo);          
    }

    @UseGuards(JwtGuard)
    @Put('update')
    async update(
      @Body() todo: TodoItem){
          this.todoService.updateTodo(todo);          
    }

    @UseGuards(JwtGuard)
    @Delete('delete/:id')    
    async delete(
      @Param('id') id: string
      ){        
          this.todoService.deleteTodo(id);          
    }

}
