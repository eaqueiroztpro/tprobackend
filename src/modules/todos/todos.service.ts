import { Injectable, Inject } from '@nestjs/common';
import { TodoItem } from './todos.entity';


@Injectable()
export class TodosService {
    myTodos:Array<TodoItem>=[];

    constructor (
        @Inject('TODOS_REPOSITORY')
        private todoRepository: typeof TodoItem
    ){ }

    private newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    }

    getAllTodos(){
        return this.todoRepository.findAll();
    }

    createTodo(todo:TodoItem) {    
        todo.id = this.newGuid();
        this.todoRepository.create({
            id: todo.id,
            description: todo.description,
            done: todo.done
        }); 
    }    

    updateTodo(todo:TodoItem){        
        return this.todoRepository.findOne({where:{id:todo.id}})
                                  .then((usr)=> {
                                      if(usr){
                                          usr.update({
                                              description:todo.description,
                                              done: todo.done
                                          })
                                      }
                                  });

        // let index = this.myTodos.findIndex(x=> x.id == todo.id);
        // this.myTodos[index] = todo;        
    }

    deleteTodo(todoId:string){
        this.todoRepository.destroy({
            where:{
                id:todoId
            }
        })

        // let index = this.myTodos.findIndex(x=> x.id == todoId);
        // if(index > -1)
        // this.myTodos.splice(index,1);        
    }
}


