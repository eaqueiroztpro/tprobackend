import { Test, TestingModule } from '@nestjs/testing';
import { TodosController } from './todos.controller';
import { TodosService } from './todos.service';

describe('TodosController', () => {
  let controller: TodosController;
  
  const myTodoItemList =[{ myItem:"myItem"  }]
  const fakeTodoService = {      
      provide: TodosService,
      useFactory: ()=>({
        getAllTodos: jest.fn(()=> myTodoItemList)
      })    
  }

  const fakeRepo = {
    provide: 'TODOS_REPOSITORY',
    useValue: {
      emit: jest.fn(),
    },
  }
  
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TodosController],
      providers: [ fakeTodoService,
                   fakeRepo                    
                 ], 
    }).compile();

    controller = module.get<TodosController>(TodosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('Get all Todos should return Empty', () => {  
    return controller.getAll().then( data => {
      expect(data).toBe(myTodoItemList);
    })    
  });
});
