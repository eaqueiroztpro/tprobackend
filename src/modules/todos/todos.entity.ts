import { ApiProperty } from '@nestjs/swagger';
import { Column, Model, PrimaryKey, Table } from 'sequelize-typescript';


@Table
export class TodoItem extends Model{    
    @ApiProperty()
    @PrimaryKey
    @Column
    id: string;

    @ApiProperty()
    @Column
    description: string;

    @ApiProperty()
    @Column
    done: boolean;
}