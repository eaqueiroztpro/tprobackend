<p align="center">
  TPro Backend Test
</p>

## Description

A Backend Server build using: Node JS, Nest Js, Postgress DB, Docker

## Test

```bash
# Before build the docker image please test the code by running the following command:
$ npm run test
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# create the docker Image
$ docker build -t tpro-backend-image .

# compile the docker compose
$ docker-compose build

# run database and backend
$ docker-compose up

#Swagger Page
open your browser and check the URL -> http://localhost:3000/api/

```


## Stay in touch

- Author - Eduardo Queiroz (eaqueiroz@gmail.com)

